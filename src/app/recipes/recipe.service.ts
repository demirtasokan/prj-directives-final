import {EventEmitter, Injectable} from '@angular/core';
import {Recipe} from './recipe.model';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

 private recipes: Recipe[] = [
    new Recipe('Fat Burger',
      'This is simply a test',
      'http://www.burgerking.com.my/upload/image/Product/9/0-whopper-cheese-thumb.jpg',
   [
     new Ingredient('Meat', 1),
     new Ingredient('French Fries', 20)
   ]),
    new Recipe('Tasty schnitzel',
      'This is simply a test',
      'http://www.burgerking.com.my/upload/image/Product/68/0-salad-thumb.jpg',
      [
        new Ingredient('Buns', 21),
        new Ingredient('Meat', 2)
      ])
  ];

 constructor(private slService: ShoppingListService) {}

  getRecipes() {
   return this.recipes.slice();
 }
 addIngredientsToShoppingList(ingredients: Ingredient[]) {
   this.slService.addIngredients(ingredients);

 }
}
